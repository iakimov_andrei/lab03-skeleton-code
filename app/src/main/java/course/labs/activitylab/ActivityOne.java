package course.labs.activitylab;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class ActivityOne extends Activity {

		// string for logcat documentation
		private final static String TAG = "Lab-ActivityOne";

		// lifecycle counts
		//TODO: Create 7 counter variables, each corresponding to a different one of the lifecycle callback methods.
		//TODO:  increment the variables' values when their corresponding lifecycle methods get called.
        private  int onCreateCounter = 0;
        private int onStartCounter = 0;
        private int onResumeCounter = 0;
        private int onPauseCounter = 0;
        private int onRestartCounter = 0;
        private int onStopCounter = 0;
        private int onDestroyCounter = 0;

		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_one);

			//Log cat print out
			Log.i(TAG, "onCreate called");
			
			//TODO: update the appropriate count variable & update the view
            onCreateCounter++;
            TextView create = (TextView) findViewById(R.id.create);
            String createText = create.getText().toString();
            createText = createText.split(" ")[0];
            createText = createText + " " + onCreateCounter;
            create.setText(createText);


		}

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.activity_one, menu);
			return true;
		}
		
		// lifecycle callback overrides
		
		@Override
		public void onStart(){
			super.onStart();

			//Log cat print out
			Log.i(TAG, "onStart called");
			//TODO:  update the appropriate count variable & update the view
            onStartCounter++;
            TextView onStartView = (TextView) findViewById(R.id.start);
            String startString = onStartView.getText().toString();
            startString = startString.split(" ")[0];
            startString += " " + onStartCounter;
            onStartView.setText(startString);
		}

	    // TODO: implement 5 missing lifecycle callback methods

        @Override
        public void onResume()
        {
            super.onResume();
            Log.i(TAG, "onResume called");
            onResumeCounter++;
            TextView resumeView = (TextView) findViewById(R.id.resume);
            String resume = getResources().getString(R.string.onResume);
            resumeView.setText(resume + " " + onResumeCounter);
        }

        @Override
        public void onStop()
        {
            super.onStop();
            Log.i(TAG, "onStop called");
            onStopCounter++;
            TextView stopView = (TextView) findViewById(R.id.stop);
            String stop = stopView.getText().toString().split(" ")[0];
            stopView.setText(stop);
        }

        @Override
        public void onRestart()
        {
            super.onRestart();
            Log.i(TAG, "onRestart called");
            onRestartCounter++;
            String restart = getResources().getString(R.string.onRestart);
            TextView restartView = (TextView) findViewById(R.id.restart);
            restartView.setText(restart + " " + onRestartCounter);
        }

        @Override
        public void onPause()
        {
            super.onPause();
            Log.i(TAG, "onPause called");
            onPauseCounter++;
            String pause = getResources().getString(R.string.onPause);
            TextView pauseView = (TextView) findViewById(R.id.pause);
            pauseView.setText(pause + " " + onPauseCounter);
        }

        @Override
        public void onDestroy()
        {
            super.onDestroy();
            Log.i(TAG, "onDestroy called");
            onDestroyCounter++;
            String destroy = getResources().getString(R.string.onDestroy);
            TextView destroyView = (TextView) findViewById(R.id.destroy);
            destroyView.setText(destroy + " " + onDestroyCounter);
        }
	    // Note:  if you want to use a resource as a string you must do the following
	    //  getResources().getString(R.string.stringname)   returns a String.

		@Override
		public void onSaveInstanceState(Bundle savedInstanceState){
			//TODO:  save state information with a collection of key-value pairs & save all  count variables

            savedInstanceState.putInt("onCreateCounter", onCreateCounter);
            savedInstanceState.putInt("onStartCounter", onStartCounter);
            savedInstanceState.putInt("onResumeCounter", onResumeCounter);
            savedInstanceState.putInt("onPauseCounter", onPauseCounter);
            savedInstanceState.putInt("onRestartCounter", onRestartCounter);
            savedInstanceState.putInt("onStopCounter", onStopCounter);
            savedInstanceState.putInt("onDestroyCounter", onDestroyCounter);

		}

		@Override
        public void onRestoreInstanceState(Bundle savedInstanceState)
        {
            onCreateCounter = savedInstanceState.getInt("onCreateCounter");
            onStartCounter = savedInstanceState.getInt("onStartCounter");
            onResumeCounter = savedInstanceState.getInt("onResumeCounter");
            onPauseCounter = savedInstanceState.getInt("onPauseCounter");
            onRestartCounter = savedInstanceState.getInt("onRestartCounter");
            onStopCounter = savedInstanceState.getInt("onStopCounter");
            onDestroyCounter = savedInstanceState.getInt("onDestroyCounter");
        }


		public void launchActivityTwo(View view) {
			startActivity(new Intent(this, ActivityTwo.class));
		}
		

}
